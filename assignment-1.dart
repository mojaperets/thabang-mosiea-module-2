void main() {
  String myName = "Thabang Sylvester";
  String myFavApp = "Color Switch";
  String myCity = "Aliwal North";

  print(
      "Hi. My name is ${myName}. My favourite app is ${myFavApp} and I'm from ${myCity}.");
}
