void main() {
  //Part a)
  AppOfTheYear appObj = new AppOfTheYear();
  print('Name of app: ' +
      appObj.name +
      '\n' +
      'Category of app: ' +
      appObj.category +
      '\n' +
      'Developer: ' +
      appObj.developer +
      '\n' +
      'Year won: ' +
      appObj.year.toString() +
      '\n');

  //Print to Caps
  appObj.transToCapital();
}

class AppOfTheYear {
  String name = 'Ambani Africa';
  String category = 'Best South African Solution, Best Gaming Solution and' +
      ' Best Educational Solution';
  String developer = 'Mukundi Lambani';
  int year = 2021;

  //Part b
  void transToCapital() {
    String appName = name.toUpperCase();
    print(appName);
  }
}
