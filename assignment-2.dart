void main() {
  List<String> arrWinningApps = [
    'Takealot.com (2021)',
    'Guardian Health (2020)',
    'Hydra (2019)',
    'African Snake Bite (2018)',
    'OrderIn (2017)',
    'Tuta-me (2016)',
    'M4JAM (2015)',
    'RV (2014)',
    'Bookly (2013)',
    'Discovery HealthID (2012)'
  ];

  //Part a)
  arrWinningApps.sort();
  arrWinningApps.forEach((element) => print(element));
  //Newline spacing
  print('\n');

  //Part b)
  print('Winning app for 2017 - ' + arrWinningApps[6]);
  print('Winning app for 2018 - ' + arrWinningApps[0]);

  //
  print('\n');
  int totalApps = arrWinningApps.length;
  print('Total number of apps: $totalApps');
}
